export default function AppRoute($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/orquestrator/login');

    $stateProvider
        .state('orquestrator', {
            abstract: true,
            cache: false,
            url: '/orquestrator',
            controller: 'AppController as vm',
            templateUrl: 'src/app/template.html'
        })
}