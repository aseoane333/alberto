import login from './login';
import home from './home';
import register from './register';

export default angular.module('orquestrator.features', [
    login,
    home,
    register
]).name;