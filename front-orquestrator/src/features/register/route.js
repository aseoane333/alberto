export default function RegisterRoute($stateProvider) {
    $stateProvider
        .state('register', {
            parent: 'orquestrator',
            cache: false,
            url: '/register',
            views: {
                'bodyView': {
                    templateUrl: "src/features/register/template.html",
                    controller: 'RegisterController as vm'
                }
            }
        })
}