import route from './route';
import controller from './controller';

export default angular
    .module('orquestrator.register', [
        'ui.router'
    ])
    .config(route)
    .controller('RegisterController', controller)
    .name;