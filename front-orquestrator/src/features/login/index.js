import route from './route';
import controller from './controller';

export default angular
    .module('orquestrator.login', [
        'ui.router'
    ])
    .config(route)
    .controller('LoginController', controller)
    .name;