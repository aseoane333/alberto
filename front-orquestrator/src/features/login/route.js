export default function LoginRoute($stateProvider) {
    $stateProvider
        .state('login', {
            parent: 'orquestrator',
            cache: false,
            url: '/login',
            views: {
                'bodyView': {
                    templateUrl: "src/features/login/template.html",
                    controller: 'LoginController as vm'
                }
            }
        })
}