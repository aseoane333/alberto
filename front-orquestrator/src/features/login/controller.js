export default function LoginController($state, $form, $user, $log, CONSTANTS) {
    'ngInject'

    let vm = this;
    let credentialsReady;

    vm.credentials = {
        email: 'aseoane333@gmail.com',
        password: '1234'
    };

    vm.login = login;
    vm.register = register;

    function login() {
        if($form.isEmptyCredentials(vm.credentials)) {
            return $log.info(CONSTANTS.label.emptyForm);
        }

        $user.getUser(vm.credentials)
            .then((user) => $state.go('home'))
            .catch((err) => $log.info(CONSTANTS.label.wrongCredentials, err));
    }

    function register() {
        if($form.isEmptyCredentials(vm.credentials)) {
            return $log.info(CONSTANTS.label.emptyForm);
        }

        $state.go('home');
    }
}