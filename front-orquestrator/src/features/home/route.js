export default function HomeRoute($stateProvider) {
    $stateProvider
        .state('home', {
            parent: 'orquestrator',
            cache: false,
            url: '/home',
            views: {
                'bodyView': {
                    templateUrl: "src/features/home/template.html",
                    controller: 'HomeController as vm'
                }
            }
        })
}