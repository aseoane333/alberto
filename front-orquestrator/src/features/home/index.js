import route from './route';
import controller from './controller';

export default angular
    .module('orquestrator.home', [
        'ui.router'
    ])
    .config(route)
    .controller('HomeController', controller)
    .name;