export default {
    "URL": {
        "DEV": "http://localhost:4003",
        "PRO": "HEROKU / CLOUDFROUNDRY / AMAZON / AZURE",
    },
    "PATH": {
        "PARENT": "/api/public",
        "USER": {
            "LOGIN": "/login/user",
            "REGISTER": "/register/user"
        }
    },
    "label": {
        "emptyForm": "Hay algun campo vacio",
        "wrongCredentials": "Usuario y/o contraseña incorrectos"
    }
}
