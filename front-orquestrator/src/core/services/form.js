export default function () {
    'ngInject'

    this.isEmptyCredentials = isEmptyCredentials;

    function isEmptyCredentials(credentials) {
        return credentials.email === '' || credentials.password === '';
    }
}