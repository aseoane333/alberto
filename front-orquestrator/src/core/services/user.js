export default function ($http, $q, $coreStorage, CONSTANTS) {
    'ngInject'

    const URL_DEV = CONSTANTS.URL.DEV + CONSTANTS.PATH.PARENT;

    this.getUser = getUser;

    function getUser(user) {
        const q = $q.defer();

        $http.post(URL_DEV + CONSTANTS.PATH.USER.LOGIN, user).then((user) => {   
            $coreStorage.set('user', user.data);
            q.resolve(user);
        }).catch((error) => q.reject(error));

        return q.promise;
    }
}