export default function () {
    'ngInject'

     this.set = function (key, value) {
         localStorage.setItem(key, btoa(JSON.stringify(value)));
     }

     this.get = function (key) {
         let storage = localStorage.getItem(key) !== null
            ? JSON.parse(atob(localStorage.getItem(key)))
            : undefined;
        
        return storage;
     }

     this.remove = function (key) {
         return localStorage.removeItem(key);
     }
}