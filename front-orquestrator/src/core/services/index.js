import user from './user';
import form from './form';
import coreStorage from './storage';

export default angular
    .module('core.service', [

    ])
    .service('$coreStorage', coreStorage)
    .service('$user', user)
    .service('$form', form)
    .name;