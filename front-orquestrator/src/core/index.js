// import components from './components';
import services from './services';
import constants from './constants';

export default angular
    .module('orquestrator.core', [
        // components,
        services
    ])
    .constant('CONSTANTS', constants)
    .name;